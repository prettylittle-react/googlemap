import React from 'react';
import PropTypes from 'prop-types';

import Settings from 'settings';
import EventEmitter from 'eventemitter';

import './GoogleMap.scss';

/**
 * GoogleMap
 * @description [description]
 * @example
  <div id="GoogleMap"></div>
  <script>
	ReactDOM.render(React.createElement(Components.GoogleMap, {
	}), document.getElementById("GoogleMap"));
  </script>
 */
class GoogleMap extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'google-map';
	}

	pinSymbol(colour) {
		return {
			path:
				'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
			fillColor: colour,
			fillOpacity: 1,
			strokeColor: '#d8d8d8',
			strokeWeight: 0,
			scale: 1
		};
	}

	onInit = () => {
		const {lat, lng, zoom, markers, styler} = this.props;

		const map = new google.maps.Map(this.map, {
			center: {
				lat,
				lng
			},
			zoom,
			scrollwheel: false,
			streetViewControl: false,
			mapTypeControlOptions: {
				mapTypeIds: []
			}
		});

		if (styler) {
			const styledMapType = new window.google.maps.StyledMapType(styler, {
				name: 'Styled Map'
			});
			map.mapTypes.set('styled_map', styledMapType);
			map.setMapTypeId('styled_map');
		}

		if (markers) {
			[].forEach.call(markers, markerData => {
				const marker = new window.google.maps.Marker({
					position: {lat: markerData.lat, lng: markerData.lng},
					title: markerData.title || '',
					animation: google.maps.Animation.DROP,
					icon: this.pinSymbol('#000000'),
					map
				});

				if (markerData.content) {
					const infowindow = new window.google.maps.InfoWindow({
						content: markerData.content
					});

					marker.addListener('click', () => {
						infowindow.open(map, marker);
					});
				}
			});
		}
	};

	get willRender() {
		const {lng, lat} = this.props;

		return !(lat === null || lng === null);
	}

	componentDidMount() {
		const {apiKey} = this.props;

		const key = apiKey || Settings.config.googleMapsApiKey;

		if (!key || SERVER || !this.willRender) {
			return null;
		}

		if (!window._googleMapCallback) {
			window._googleMapCallback = function() {
				EventEmitter.dispatch('maps-loaded');
			};

			const script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = `https://maps.googleapis.com/maps/api/js?key=${key}&callback=_googleMapCallback`;

			document.body.appendChild(script);
		}

		EventEmitter.subscribe('maps-loaded', this.onInit);
	}

	render() {
		if (!this.willRender) {
			return null;
		}
		return <div ref={map => (this.map = map)} className={this.baseClass} />;
	}
}

GoogleMap.defaultProps = {
	apiKey: '',
	lat: null,
	lng: null,
	zoom: 15,
	markers: null,
	styler: null
};

GoogleMap.propTypes = {
	apiKey: PropTypes.string,
	lat: PropTypes.number.isRequired,
	lng: PropTypes.number.isRequired,
	markers: PropTypes.array,
	styler: PropTypes.array,
	zoom: PropTypes.number
};

export default GoogleMap;
